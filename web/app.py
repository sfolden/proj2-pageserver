from flask import Flask
from flask import *
import os.path

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"



#main function that handles requests
@app.route("/<string:webpage_name>")
def do_everything(webpage_name):
    if os.path.exists("./templates/" +webpage_name): #check if page exists in templates folder
        return render_template(webpage_name), 200 #if so returns page
    bad_strings = ["~", "//", ".."] #forbidden strings list
    if bad_strings[0] in webpage_name or bad_strings[1] in webpage_name or bad_strings[2] in webpage_name: #checks for forbidden strings
        abort(403) #if forbidden string found, abort,  throw 403
    else:
        abort(404)  #else cant find page, throw 404


#throws 403
@app.errorhandler(403)
def forbidden(e):
    return render_template("403.html"), 403


#throws 404
@app.errorhandler(404)
def not_found(e):
  return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
    #print("were live")
